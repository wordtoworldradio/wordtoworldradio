
Rails.application.routes.draw do  
  devise_for :users, :controllers => {registrations: 'registrations',sessions: 'sessions',omniauth_callbacks: "omniauth_callbacks"}
  resources :sliders

  resources :news
  #get 'friends/new'
  resources :mailbox   
  resources :friends  
  resources :contacts 

  resources :categories do
    resources :bibles do
      resources :chapters
    end
  end

  # resources :bibles do
  #   resources :chapters
  # end

  get 'radio/index'

  resources :audios 

  resources :albums do
    resources :audios  
  end 
  get 'welcome/album_url'
  get 'welcome/video_details'
  get 'welcome/audio_details'
  get 'welcome/album_details'
  resources :daily_breads
  resources :videos
  root 'welcome#index'
  get 'welcome/index'
  get 'welcome/contact'
    get 'welcome/about'
  # devise_for :admins  
  devise_for :admins, path: "admin", path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', unlock: 'unblock', registration: 'register', sign_up: 'cmon_let_me_in' }
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
 
end
