# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0' 
Rails.application.config.assets.precompile += %w(autocomplete-rails.js sticky/waypoints.min.js sticky/waypoints-sticky.js core/bootstrap.min.js
sticky/classie.js
sticky/cbpAnimatedHeader.min.js
slider/owl.carousel.min.js
slider/sly/sly.js
slider/newsscroll/jquery.newsTicker.js
slider/textillate/jquery.lettering.js
slider/textillate/jquery.textillate.js)

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
