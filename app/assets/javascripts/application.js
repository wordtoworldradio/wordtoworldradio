// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//= require jquery
//= require jquery_ujs  
//= require owl.carousel
//= require_tree .  

$(document).ready(function() { 
    $.SHOUTcast({
   host : 'stardust.wavestreamer.com',
   port : 8062,
  interval : 5000,
   }).stats(function(){
      $('#songtitle').text('title:' + this.get('songtitle'));
      $('#status').text('status:' + this.get('status'));
      $('#currentlisteners').text('currentlisteners:' + this.get('currentlisteners'));
      $('#peaklisteners').text(this.get('peaklisteners'));
      $('#streamhits').text(this.get('streamhits'));
      $('#nexttitle').text('next title:' + this.get('nexttitle'));
      $('#content').text(this.get('content'));

   });
}); 
