class WelcomeController < ApplicationController
  def index
  	@prayerreq = Contact.where(:contacttype => 1)
    @testimonials = Contact.where(:contacttype => 2)
  	@sliders = Slider.all
  	@albums=Album.all
  	@albums_random=Album.order("RANDOM()")

  	
    @videos = Video.all
    @video_first=Video.first
  	@album_first=Album.first
  end

  def contact
  end

  def about
  end
  def audio_details
     
    puts params
     data =  Chapter.find(params[:chapter_id])
    render :json => data, :status => :ok
  end

  def album_details 
 
    data =  Album.find(params[:album_id])  
    render :json => data 
  end

  def album_url
    data =  Album.find(params[:album_id]) 
    render :json => data 
  end

  def video_details 
    data =  Video.find(params[:video_id])
    render :json => data, :status => :ok
  end

end
