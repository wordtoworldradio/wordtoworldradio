class BiblesController < ApplicationController
  before_action :set_bible, only: [:show, :edit, :update, :destroy] 
  
  def index     
    @bibles = Bible.search(params[:search])    
  end
  def show
    
    @categories = Category.all 
    if params[:category_id].to_i ==0
       @bibles = Bible.only_with_chapters
    else
       @category = Category.find(params[:category_id].to_i)
       @bibles = @category.bibles.only_with_chapters 
    end
    @bible = Bible.find(params[:id])
    @album_bottom_pagiantion  = Album.paginate(:page => params[:page], :per_page => 2)
    @chapters = @bible.chapters.paginate(:page => params[:page], :per_page => 2)
    a = @bible.chapters.count
     b = (1..a).to_a.map{ |a| [a,a] }
     @number_array = b.flatten.take(a)
    #redirect_to bible_chapters_path(@bible)  
  end

  def new
    redirect_to bibles_path
  end  

  def edit
    redirect_to bibles_path 
  end

  def update
    redirect_to bibles_path
  end

  
  
  private    
    def set_bible
      @bible = Bible.find(params[:id])
    end    
    def bible_params
      params.require(:bible).permit(:name, :bible_cover, :admin_id, :status)
    end
end
