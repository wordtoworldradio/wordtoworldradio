class SessionsController < Devise::SessionsController
    
  def create
     self.resource = warden.authenticate(auth_options)
  if resource && resource.active_for_authentication?
     respond_to do |format|
      format.html {
      sign_in(resource_name, resource)
      flash[:notice] = "Created account, signed in."
      redirect_to root_path
    }
    format.js {
     flash[:notice] = "Created account, signed in."
     render :template => "shared/devise_success_signin.js.erb"
      
     sign_in(resource_name, resource)
   }
     end
  else
     

     respond_to do |format|
       
    format.js {
     flash[:notice] = "Created account, signed in."
     render :template => "shared/devise_errors_sign_in.js.erb"
      
      
   }
     end


  end
  	 
  end

  def failure
    return render:json => {:success => false, :errors => ["Login failed."]}
  end
end
