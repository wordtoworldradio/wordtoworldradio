class VideosController < ApplicationController
  before_action :set_video, only: [:show, :edit, :update, :destroy]
   before_filter :loged_in?, :only => [:new,:create, :edit, :destroy]

  # GET /videos
  # GET /videos.json
  def index
    # @videos = Video.all
    #  @videos_bottom_pagiantion  = Video.paginate(:page => params[:page], :per_page => 3)
    if params[:search] 
      @videos = params[:search].blank? ? Video.paginate(:page => params[:page], :per_page => 10) : Video.search(params[:search]).order("created_at DESC") 
    else
      @videos = Video.paginate(:page => params[:page], :per_page => 10)
      @videos_bottom_pagiantion  = Video.paginate(:page => params[:page], :per_page => 10)
    end 

  end

  # GET /videos/1
  # GET /videos/1.json
  def show
    @videosall = Video.all
    @video_first=Video.first
    @album_first=Album.first
    
    @video = Video.find(params[:id])
    @videos=Video.where.not(id: @video.id)
     @videos_bottom_pagiantion  = Video.paginate(:page => params[:page], :per_page => 3)
  end

  # GET /videos/new
  def new
    @video = Video.new
  end

  # GET /videos/1/edit
  def edit
  end
  
  def contact
    video      = params[:video]
    respond_to do |format|
    format.json { render :json => { :video => video} }
    end
  end

  # POST /videos
  # POST /videos.json
  def create
    @video = Video.new(video_params)

    respond_to do |format|
      if @video.save
        format.html { redirect_to @video, notice: 'Video was successfully created.' }
        format.json { render :show, status: :created, location: @video }
      else
        format.html { render :new }
        format.json { render json: @video.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /videos/1
  # PATCH/PUT /videos/1.json
  def update
    respond_to do |format|
      if @video.update(video_params)
        format.html { redirect_to @video, notice: 'Video was successfully updated.' }
        format.json { render :show, status: :ok, location: @video }
      else
        format.html { render :edit }
        format.json { render json: @video.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /videos/1
  # DELETE /videos/1.json
  def destroy
    @video.destroy
    respond_to do |format|
      format.html { redirect_to videos_url, notice: 'Video was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_video
      @video = Video.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def video_params
      params.require(:video).permit(:name, :video)
    end
    def loged_in?
      redirect_to root_path
    end
end
