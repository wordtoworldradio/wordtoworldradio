module ApplicationHelper
	def resource_name
      :user
	end

	def resource
	 @resource ||= User.new
	end

	def devise_mapping
	 @devise_mapping ||= Devise.mappings[:user]
	end

	def sharable_image 
	    image_path = "word-to-world-radio-logo.png"  
	end

	def title(page_title)
	  content_for(:title) { page_title }
	end 
end