# == Schema Information
#
# Table name: contacts
#
#  id          :integer          not null, primary key
#  name        :string
#  email       :string
#  message     :text
#  contacttype :integer
#  approval    :boolean
#  status      :boolean
#  admin_id    :integer
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_contacts_on_admin_id  (admin_id)
#

class Contact < ActiveRecord::Base
  belongs_to :admin
   validates :name, presence: true
   validates :message, presence: true
   validates :email, format: { with: /\A[^@\s]+@([^@.\s]+\.)+[^@.\s]+\z/ }

   def date  
   	 self.created_at.to_date
   end
end
