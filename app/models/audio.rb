# == Schema Information
#
# Table name: audios
#
#  id         :integer          not null, primary key
#  name       :string
#  audio      :string
#  album_id   :string
#  artist     :string
#  created_at :datetime
#  updated_at :datetime
#

class Audio < ActiveRecord::Base
	mount_uploader :audio, AudioUploader
	belongs_to :album
	
	 def self.search(query)
   	 where("name like ?", "%#{query}%") 
 	 end	

 	 validates :artist, :presence => true
 	 validates :audio, :presence => true
 	 validates :name, :presence => true
end
