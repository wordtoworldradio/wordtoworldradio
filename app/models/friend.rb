# == Schema Information
#
# Table name: friends
#
#  id           :integer          not null, primary key
#  name         :string
#  friend_email :string
#  created_at   :datetime
#  updated_at   :datetime
#

class Friend < ActiveRecord::Base
   validates :name, presence: true
   validates :friend_email, presence: true
   validates :friend_email, format: { with: /\A[^@\s]+@([^@.\s]+\.)+[^@.\s]+\z/ }
end
