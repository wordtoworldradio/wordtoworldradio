# == Schema Information
#
# Table name: videos
#
#  id         :integer          not null, primary key
#  name       :string
#  video      :string
#  created_at :datetime
#  updated_at :datetime
#

class Video < ActiveRecord::Base
	mount_uploader :video, VideoUploader

	def set_success(format, opts)
    	self.success = true
    end
     validates :name, :presence => true
 	 validates :video, :presence => true

 	 def self.search(search)
   	  where("name like ?", "%#{search}%") 
 	 end
end
