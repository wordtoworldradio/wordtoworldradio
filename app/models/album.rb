# == Schema Information
#
# Table name: albums
#
#  id         :integer          not null, primary key
#  name       :string
#  cover      :string
#  created_at :datetime
#  updated_at :datetime
#  banner     :string
#

class Album < ActiveRecord::Base
	mount_uploader :cover, CoverUploader
	mount_uploader :banner, BannerUploader
	has_many :audios

	 def self.search(search)
   	  where("name like ?", "%#{search}%") 
 	 end
 	 validates :cover, :presence => true
 	 validates :banner, :presence => true
 	 validates :name, :presence => true

 	 def as_json(options={})
	  super(:only => [:album_id,:cover],
	        :include =>  :audios
	  )
	end
end
